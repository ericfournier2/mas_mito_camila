# Generate trim jobs
for i in raw/HI*R1.fastq.gz
do
    R1File=$i
    R2File=`echo  $R1File | sed -e 's/R1/R2/'`
    sample=`echo $i | sed -e 's/raw\/HI.*.Index_[0-9]*\\.//' | sed -e 's/_R1.fastq.gz//'`
    sample=`echo $sample | sed -e 's/---reverse_i5adapter_fakeindex./Run2_/'`
    sample=`echo $sample | sed -e 's/^\./Run1_/'`
    script=output/jobs/trim.$sample.sh
    mkdir -p output/jobs
    mkdir -p output/trim
    cat <<EOF > $script
#!/bin/bash
module load trimmomatic && \
mkdir -p output/trim && \
java -XX:ParallelGCThreads=1 -Xmx20G -jar \$EBROOTTRIMMOMATIC/trimmomatic-0.36.jar PE \
  -threads 6 \
  -phred33 \
  $R1File $R2File output/trim/$sample.R1.fastq.gz output/trim/$sample.R1.unpaired.fastq.gz output/trim/$sample.R2.fastq.gz output/trim/$sample.R2.unpaired.fastq.gz \
  ILLUMINACLIP:input/adapters.fa:2:30:15 \
  TRAILING:30 HEADCROP:4 \
  MINLEN:25
EOF
    sbatch --time 10:00:00 --mem 20G --cpus-per-task 6 --account def-masirard -D `pwd` -e $script.stderr -o $script.stdout $script
done