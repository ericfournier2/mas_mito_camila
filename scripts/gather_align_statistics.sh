for i in output/bismark_MT_Spike/*/*R1_bismark_bt2_PE_report.txt
do     
    sample=`basename $i .R1_bismark_bt2_PE_report.txt`
    total=`grep -F 'Sequence pairs analysed in total:' $i |
           sed -e 's/Sequence pairs analysed in total://'`
    align=`grep -F 'Number of paired-end alignments with a unique best hit:' $i |
           sed -e 's/Number of paired-end alignments with a unique best hit://'`
    echo $sample $total $align
done
    