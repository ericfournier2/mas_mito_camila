#!/bin/bash
module load samtools

mkdir -p output/imprinted
for bam in output/bismark/*/*.bam
do
    sample=`basename $bam .R1_bismark_bt2_pe.bam`

    samtools view -b -h -L input/imprinted.bed $bam > output/imprinted/$sample.bam
done

module load mugqic/bismark
module load bowtie2

# Generate bismark alignment jobs
for bam in output/imprinted/*.bam
do
    sample=`basename $bam .bam`

    mkdir -p output/imprinted/$sample
    bismark_methylation_extractor --CX_context -o output/imprinted/$sample  --gzip --bedgraph $bam
done
