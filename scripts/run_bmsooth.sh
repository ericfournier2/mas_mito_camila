#!/bin/bash
for i in `seq 1 24`
do
    script=output/jobs/smooth.$i.sh
    cat << EOF > $script
#!/bin/bash
   
module load r
Rscript scripts/parallel_smooth.R $i
EOF

    sbatch -D `pwd` -o $script.stdout -e $script.stderr --time 6:00:00 --cpus-per-task 1 --mem 32G --account def-masirard $script
done

