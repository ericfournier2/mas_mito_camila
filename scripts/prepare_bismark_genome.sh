#!/bin/bash
#SBATCH --time=6:00:00
#SBATCH --mem=96G
#SBATCH --cpus-per-task=2
#SBATCH --account=def-masirard

module load mugqic/bismark
module load samtools
module load bowtie2

bismark_genome_preparation --verbose output/bismark_genome