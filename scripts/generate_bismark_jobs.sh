# Generate bismark alignment jobs
for r1 in output/trim/*.R1.fastq.gz
do
    sample=`basename $r1 .R1.fastq.gz`
    r2=`echo $r1 | sed -e 's/R1/R2/'`

    mkdir -p output/jobs
    script=output/jobs/$sample.bismark.sh
    if [ ! -e output/bismark/$sample/$sample.R1_bismark_bt2_PE_report.txt ]
    then
        cat <<EOF > $script
#!/bin/bash
module load mugqic/bismark
module load samtools
module load bowtie2

mkdir -p output/bismark/$sample
bismark -o output/bismark/$sample --non_directional --multicore 6  output/bismark_genome -1 $r1 -2 $r2
EOF

        sbatch --time 8:00:00 --mem 90G --cpus-per-task 24 --account def-masirard -D `pwd` -e $script.stderr -o $script.stdout $script
    fi
done
