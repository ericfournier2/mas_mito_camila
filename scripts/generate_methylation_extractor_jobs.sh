# Generate bismark alignment jobs
for bam in output/bismark/*/*.bam
do
    sample=`basename $bam .R1_bismark_bt2_pe.bam`

    mkdir -p output/jobs
    script=output/jobs/$sample.methylation_extractor.sh
    if [ ! -e output/methylation_extractor/$sample/$sample.R1_bismark_bt2_pe.bismark.cov.gz ]
    then
        cat <<EOF > $script
#!/bin/bash
module load mugqic/bismark
module load samtools
module load bowtie2

mkdir -p output/methylation_extractor/$sample
bismark_methylation_extractor -o output/methylation_extractor/$sample  --multicore 6 --gzip --bedgraph $bam
EOF

        sbatch --time 6:00:00 --mem 90G --cpus-per-task 20 --account def-masirard -D `pwd` -e $script.stderr -o $script.stdout $script
    fi
done
