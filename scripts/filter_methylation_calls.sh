for i in output/methylation_extractor/*/*_bismark_bt2_pe.bismark.cov.gz
do 
    zcat $i | grep -e "^[0-9XYML]" | gzip -c > $i.filtered.gz
done
